<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * Выданные ключи
 *
 * @property string $access_key
 * @property string $service
 * @property string $expires_in
 */
class IssuedAccessKeys extends ActiveRecord {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['access_key', 'service', 'expires_in'], 'required'],
            [['expires_in'], 'integer'],
            [['access_key'], 'string', 'max' => 64],
            [['service'], 'string', 'max' => 20]
        ];
    }
}
