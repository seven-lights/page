<?php

namespace app\models\forms;

use app\models\Page;
use app\models\Template;
use Yii;

/**
 * @inheritdoc
 */
class PageForm extends Page
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nick', 'title', 'text'], 'required'],
	        ['text', 'string'],
	        ['template_id', 'integer'],
	        [['nick', 'title'], 'string', 'max' => 100],
	        [['keywords'], 'string', 'max' => 128],
	        [['description'], 'string', 'max' => 250],
	        ['template_id', 'filter', 'filter' => function($value) {
		        if(!$value) {
			        return null;
		        }
		        return $value;
	        }],
	        [
		        'template_id',
		        'exist',
		        'targetClass' => Template::className(),
		        'targetAttribute' => [
			        'template_id' => 'id',
			        'site_id' => 'site_id'
		        ]
	        ],
        ];
    }
}
