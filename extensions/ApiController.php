<?php
/**
 * Project: Blog Platform - Seven Lights
 * User: Evgeny Rusanov
 * E-mail: admin@dudev.ru
 * Site: dudev.ru
 */

namespace app\extensions;


use app\models\Site;
use general\controllers\api\Controller;

/**
 * Контроллер-родитель для api-контроллеров
 * @property Site $site
 */
class ApiController extends Controller {
	/* @var Site $_site */
	private  $_site;
	protected $_except_action_check_domain = [];

	public function beforeAction($action) {
		if (parent::beforeAction($action)) {
			if(in_array($action->id, $this->_except_action_check_domain, true)) {
				return true;
			}
			if (!$this->getSite()) {
				echo json_encode($this->sendError(self::ERROR_NO_SITE));
				return false;
			}
			return true;
		} else {
			return false;
		}
	}
	public function getSite() {
		if($this->_site) {
			return $this->_site;
		}
		return $this->_site = Site::findOne(['domain' => \Yii::$app->request->get('domain')]);
	}
} 