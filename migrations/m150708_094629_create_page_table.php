<?php

use yii\db\Schema;
use yii\db\Migration;

class m150708_094629_create_page_table extends Migration
{
    public function up()
    {
	    $this->createTable('page', [
		    'id' => Schema::TYPE_PK,
		    'nick' => Schema::TYPE_STRING . '(100) NOT NULL',
		    'title' => Schema::TYPE_STRING . '(100) NOT NULL',
		    'keywords' => Schema::TYPE_STRING . '(128) NOT NULL',
		    'description' => Schema::TYPE_STRING . '(250) NOT NULL',
		    'text' => Schema::TYPE_TEXT . ' NOT NULL',
		    'template_id' => Schema::TYPE_INTEGER,
		    'site_id' => Schema::TYPE_INTEGER . ' NOT NULL',
		    'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
		    'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL',
	    ]);
    }

    public function down()
    {
        echo "m150708_094629_create_page_table cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
