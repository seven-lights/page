<?php

use yii\db\Schema;
use yii\db\Migration;

class m150708_103332_add_FK extends Migration
{
    public function up()
    {
	    $this->addForeignKey('site_id_FK_page', 'page', 'site_id', 'site', 'id', 'CASCADE', 'CASCADE');
	    $this->addForeignKey('template_id_FK_page', 'page', 'template_id', 'template', 'id', 'SET NULL', 'CASCADE');
    }

    public function down()
    {
        echo "m150708_103332_add_FK cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
