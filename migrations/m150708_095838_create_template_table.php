<?php

use yii\db\Schema;
use yii\db\Migration;

class m150708_095838_create_template_table extends Migration
{
    public function up()
    {
	    $this->createTable('template', [
		    'id' => Schema::TYPE_PK,
		    'name' => Schema::TYPE_STRING . '(100) NOT NULL',
		    'head' => Schema::TYPE_TEXT . ' NOT NULL',
		    'body_start' => Schema::TYPE_TEXT . ' NOT NULL',
		    'body_end' => Schema::TYPE_TEXT . ' NOT NULL',
		    'site_id' => Schema::TYPE_SMALLINT . ' NOT NULL',
		    'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
		    'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL',
	    ]);
    }

    public function down()
    {
        echo "m150708_095838_create_template_table cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
