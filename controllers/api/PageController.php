<?php

namespace app\controllers\api;

use app\extensions\ApiController;
use app\models\forms\PageForm;
use app\models\Page;

class PageController extends ApiController {
	public $defaultAction = 'list';
	public function actionCreate() {
		if(\Yii::$app->request->isGet) {
			return $this->sendError(self::ERROR_ILLEGAL_REQUEST_METHOD);
		}
		$model = new PageForm();

		if(!$model->load(\Yii::$app->request->post())) {
			return $this->sendError(self::ERROR_NO_DATA);
		}
		$model->site_id = $this->site->id;

		if($model->validate()) {
			if(!$model->save(false)) {
				return $this->sendError(self::ERROR_DB);
			}
			return $this->sendSuccess(['page' => $model->getAttributes()]);
		} else {
			$errors = $this->getErrorCodes([
				'nick' => self::ERROR_ILLEGAL_PAGE_NICK,
				'title' => self::ERROR_ILLEGAL_PAGE_TITLE,
				'keywords' => self::ERROR_ILLEGAL_PAGE_KEYWORDS,
				'description' => self::ERROR_ILLEGAL_PAGE_DESCRIPTION,
				'text' => self::ERROR_ILLEGAL_PAGE_TEXT,
				'template_id' => self::ERROR_ILLEGAL_PAGE_TEMPLATE,
			], $model);
			return $this->sendError($errors);
		}
	}
	public function actionUpdate($id) {
		/* @var PageForm $model */
		$model = PageForm::find()
			->where(['id' => $id])
			->andWhere(['site_id' => $this->site->id])
			->one();
		if(!$model) {
			return $this->sendError(self::ERROR_NO_PAGE);
		}
		if(\Yii::$app->request->isPost) {
			if (!$model->load(\Yii::$app->request->post(), 'Page')) {
				var_dump(\Yii::$app->request->post());
				return $this->sendError(self::ERROR_NO_DATA);
			}

			if ($model->validate()) {
				if (!$model->save(false)) {
					return $this->sendError(self::ERROR_DB);
				}
			} else {
				$errors = $this->getErrorCodes([
					'nick' => self::ERROR_ILLEGAL_PAGE_NICK,
					'title' => self::ERROR_ILLEGAL_PAGE_TITLE,
					'keywords' => self::ERROR_ILLEGAL_PAGE_KEYWORDS,
					'description' => self::ERROR_ILLEGAL_PAGE_DESCRIPTION,
					'text' => self::ERROR_ILLEGAL_PAGE_TEXT,
					'template_id' => self::ERROR_ILLEGAL_PAGE_TEMPLATE,
				], $model);
				return $this->sendError($errors);
			}
		}

		return $this->sendSuccess(['page' => $model->getAttributes()]);
	}
	public function actionDelete($id) {
		/* @var Page $post */
		$page = Page::find()
			->where(['id' => $id])
			->andWhere(['site_id' => $this->site->id])
			->one();
		if($page) {
			if($page->delete() === false) {
				return $this->sendError(self::ERROR_DB);
			} else {
				return $this->sendSuccess([]);
			}
		} else {
			return $this->sendError(self::ERROR_NO_POST);
		}
	}
    public function actionList() {
	    $res = Page::find()
		    ->select('id, title, description')
		    ->where(['site_id' => $this->site->id])
		    ->asArray()
		    ->all();

	    if($res === []) {
		    return $this->sendError(self::ERROR_DB);
	    }

	    return $this->sendSuccess([
		    'pages' => $res,
	    ]);
    }
    public function actionView($id) {
	    $model = Page::find()
		    ->select([
			    'id',
			    'nick',
			    'title',
			    'keywords',
			    'description',
			    'text',
			    'template_id',
		    ])
	        ->where(['id' => $id])
		    ->andWhere(['site_id' => $this->site->id])
		    ->with('template')
		    ->asArray()
	        ->one();
	    if(!$model) {
		    return $this->sendError(self::ERROR_NO_PAGE);
	    }
        return $this->sendSuccess(['page' => $model]);
    }
	public function actionViewByNick($nick) {
		$model = Page::find()
			->select([
				'id',
				'nick',
				'title',
				'keywords',
				'description',
				'text',
				'template_id',
			])
			->where(['nick' => $nick])
			->andWhere(['site_id' => $this->site->id])
			->with('template')
			->asArray()
			->one();
		if(!$model) {
			return $this->sendError(self::ERROR_NO_PAGE);
		}
		return $this->sendSuccess(['page' => $model]);
	}
}
