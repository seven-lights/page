<?php
/* @var $this yii\web\View */
/* @var $page app\models\Page */
$this->title = $page->title;

$this->registerMetaTag([
	'name' => 'description',
	'content' => $page->description,
], 'description');
$this->registerMetaTag([
	'name' => 'keywords',
	'content' => $page->keywords,
], 'keywords');

if($page->template_id) {
	$this->params['head'] = $page->template->head;
	$this->params['body_start'] = $page->template->body_start;
	$this->params['body_end'] = $page->template->body_end;
} else {
	$this->params['head'] = $this->params['body_start'] = $this->params['body_end'] = '';
}

echo $page->text;